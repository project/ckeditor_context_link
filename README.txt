CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------
This is a simple module that adds the ability to add links from the context
menu (right click).


REQUIREMENTS
------------
 * This module requires the CKEditor module.


INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * Enable the plugin by editing a profile: /admin/config/content/ckeditor

CONFIGURATION
-------------
The module has no menu or modifiable settings.  There is no
configuration.


MAINTAINERS
-----------
Current maintainer:
 * Albert Jankowski (albertski) - https://www.drupal.org/u/albertski
