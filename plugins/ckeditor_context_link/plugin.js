/**
 * @file
 * CKEditor Context link plugin.
 */

'use strict';

( function() {

 /**
   * The link module only adds the edit link if a word is not a link. This
   * plugin adds it on non links.
   */
  CKEDITOR.plugins.add( 'ckeditor_context_link', {
    requires: 'dialog,fakeobjects',
    init: function(editor) {
      if (editor.contextMenu) {
        editor.contextMenu.addListener( function( element ) {
          if (!element || element.isReadOnly()) {
            return null;
          }

          var anchor = CKEDITOR.plugins.link.getSelectedLink(editor);

          var menu = {};

          // Only add the item if not link. The link plugin does the same thing
          // but only if it is a link.
          if ( anchor === 'undefined' || anchor === null) {
            menu = { link: CKEDITOR.TRISTATE_OFF, unlink: CKEDITOR.TRISTATE_OFF };
          }

          return menu;
        });
      }
    },
  });

})();
